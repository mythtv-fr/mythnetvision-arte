#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

#notes
#http://videos.arte.tv/fr/do_delegate/videos/classic_archive-3974510,view,asPlayerXml.xml


__title__ ="Arte";
__author__="Jonas Fourquier" #alias SnouF de mythtv-fr.org
__version__="0.1"
__usage__ ='''
(Option Help)
> %(file)s -h
Usage: %(file)s [option][<search>]
Version: %(version)s Author: %(author)s

For details on the MythTV Netvision plugin see the wiki page at:
http://www.mythtv.org/wiki/MythNetvision

Options:
  -h, --help            show this help message and exit
  -v, --version         Display grabber name and supported options
  -S:, --search=        Search <search> for videos
  -T, --treeview        Display a Tree View of a sites videos
  -p, --page            Page of search
'''%{'file':__file__, 'version':__version__,'author':__author__}


import sys, os, locale, re
from getopt import getopt
from xml.dom import minidom
from BeautifulSoup import BeautifulSoup
from urllib import urlopen, urlencode
from datetime import datetime, timedelta
from ConfigParser import RawConfigParser

import logging
logging.basicConfig(level=logging.DEBUG,format='%(levelname)s line %(lineno)d - %(message)s')
logger = logging.getLogger(__title__)

reload(sys)
sys.setdefaultencoding('utf-8')

# INIT
# Url de base
urlSite = 'http://videos.arte.tv/'
urlSearch = 'http://videos.arte.tv/fr/do_search/videos/recherche?%s'
# Fichier de configuration
configFile = '/etc/mythtv/mythnetvision-arte.cfg'
userConfigFile = os.environ['HOME']+'/.mythtv/mythnetvision-arte.cfg'
# Lecteur externe devant être utilisé ('' pour le lecteur interne)
player = False
# Argument du lecteur (voir http://www.mythtv.org/wiki/MythNetvision_Grabber_Script_Format, '' pour aucun)
playerargs = False
# "Téléchargeur" ('' pour le téléchargeur par défaut)
download = False
# Argument du "téléchargeur" (voir http://www.mythtv.org/wiki/MythNetvision_Grabber_Script_Format, '' pour aucun)
downloadargs = False
# Description de channel
channel = {
        'title' : __title__,
        'link' : urlSite,
        'description' : 'Les derniers programmes de Arte'
    }


def MinidomNode_getFirstChildByTagName(self,tag) :
    '''
        Retourne le 1er enfant directe qui a pour tag "tag". Si aucun
        retourne None"
    '''
    for node in self.childNodes :
        if node.nodeType == node.ELEMENT_NODE and node.tagName == tag :
            return node
    return None
setattr(minidom.Node,'getFirstChildByTagName',MinidomNode_getFirstChildByTagName)


def MinidomNode_getChildByTagName(self,tag) :
    '''
        Retourne l'ensemble des enfants directes qui ont pour tag "tag".
        Si aucun une liste vide"
    '''
    nodes = []
    for node in self.childNodes :
        if node.nodeType == node.ELEMENT_NODE and node.tagName == tag :
            nodes.append(node)
    return nodes
setattr(minidom.Node,'getChildsByTagName',MinidomNode_getChildByTagName)


def MinidomNode_getChildByAttribute(self,attrname, attrvalue) :
    '''
        Retourne le 1er enfant directe qui a pour l'attribut "attrname" la
        valeur "attrvalue". Si aucun retourne False.
    '''
    for node in self.childNodes :
        if node.hasAttribute(attrname) :
            if node.getAttribute(attrname) == attrvalue :
                return node
    return False
setattr(minidom.Node,'getChildByAttribute',MinidomNode_getChildByAttribute)


def MinidomDocument_createNode(self,tag,text='',attributes={}) :
    '''
        Retourne un noeud avec pour tag "tag", comme text "text" ou comme
        attributs le dico "attributes" ({attrName: attrValue})
    '''
    node = self.createElement(tag)
    if text :
        node.appendChild(self.createTextNode(text))
    for (name,value) in attributes.items() :
        node.setAttribute(name,value)
    return node
setattr(minidom.Document,'createNode',MinidomDocument_createNode)


def domVersion() :
    '''
        Retourne le noeux racine permettant a mythtv de reconnaitre le nom,
        et les fonction de ce grabbeur
    '''
    domOutput = minidom.Document()
    rootNode = domOutput.appendChild(domOutput.createNode('grabber'))
    rootNode.appendChild(domOutput.createNode('name',__title__))
    rootNode.appendChild(domOutput.createNode('command',__file__))
    rootNode.appendChild(domOutput.createNode('author',__author__))
    rootNode.appendChild(domOutput.createNode('thumbnail','arte.png'))
    rootNode.appendChild(domOutput.createNode('type','video'))
    rootNode.appendChild(domOutput.createNode('description',channel['description']))
    rootNode.appendChild(domOutput.createNode('version',__version__))
    rootNode.appendChild(domOutput.createNode('search','true'))
    rootNode.appendChild(domOutput.createNode('tree','true'))
    return rootNode


def domTreeview() :
    domOutput = minidom.Document()
    nodeOutputRss = domOutput.appendChild(domOutput.createNode(
            tag='rss',
            attributes={
                    'version':'2.0',
                    'xmlns:itunes':'http://www.itunes.com/dtds/podcast-1.0.dtd',
                    'xmlns:content':'http://purl.org/rss/1.0/modules/content/',
                    'xmlns:cnettv':'http://cnettv.com/mrss/',
                    'xmlns:creativeCommons':'http://backend.userland.com/creativeCommonsRssModule',
                    'xmlns:media':'http://search.yahoo.com/mrss/',
                    'xmlns:atom':'http://www.w3.org/2005/Atom',
                    'xmlns:amp':'http://www.adobe.com/amp/1.0',
                    'xmlns:dc':'http://purl.org/dc/elements/1.1/',
                }
        ))

    # node channel
    channelOutputNode = nodeOutputRss.appendChild(domOutput.createNode('channel'))
    channelOutputNode.appendChild(domOutput.createNode('title',__title__))
    channelOutputNode.appendChild(domOutput.createNode('link',channel['link']))
    channelOutputNode.appendChild(domOutput.createNode('description',channel['description']))

    for pageName,urlPage in [
            ('Bonus', 'http://videos.arte.tv/fr/videos/toutesLesVideos'),
            ('Programmes', 'http://videos.arte.tv/fr/videos/programmes'),
            ('Événements', 'http://videos.arte.tv/fr/videos/events/index-3188672.html')
      ]:
        logger.debug('Top directory : %s'%pageName)

        soup = BeautifulSoup(urlopen(urlPage).read().replace('&#39;','\''))

        #top directory
        topDirectoryNode = channelOutputNode.appendChild(domOutput.createNode(
                tag = 'directory',
                attributes = {
                        'name':pageName,
                        'thumbnail':urlSite+soup.find('div',attrs={'class':'newVideos'}).find('div',attrs={'class':'video'}).find('img',attrs={'class':'thumbnail'}).get('src')
                    }
            ))

        #directory
        lstDivContentVideoNode = soup.findAll('div',attrs={'class':'newVideos'})

        for divContentVideoNode in lstDivContentVideoNode :
            try :
                directoryTitle = divContentVideoNode.find('h1').find('a').string
            except :
                directoryTitle = '???'
            try :
                thumbnail = urlSite+divContentVideoNode.find('div',attrs={'class':'video'}).find('img',attrs={'class':'thumbnail'}).get('src')
            except :
                thumbnail = 'http://www.arte.tv/header-include/assets/img/arte-tv.png'

            logger.debug('  Directory title : %s'%directoryTitle)

            directoryNode = topDirectoryNode.appendChild(domOutput.createNode(
                    tag = 'directory',
                    attributes = {
                            'name':directoryTitle,
                            'thumbnail':thumbnail
                        }
                ))


            lstDivVideosNodes = divContentVideoNode.findAll('div',attrs={'class':'video'})
            for divVideoNode in lstDivVideosNodes :
                try :
                    itemOutputNode = directoryNode.appendChild(domOutput.createNode('item'))

                    titleInputNode = divVideoNode.find('h2').find('a')
                    itemOutputNode.appendChild(domOutput.createNode('title',titleInputNode.string))

                    link = urlSite+titleInputNode.get('href')
                    itemOutputNode.appendChild(domOutput.createNode('link',link))

                    itemOutputNode.appendChild(domOutput.createNode('author',__title__))
                    pubDate = datetime.today().replace(hour=0,minute=0,second=0,microsecond=0)
                    for dateNode in divVideoNode.findAll('p') :
                        date = dateNode.string
                        try :
                            logger.debug('    Parse date "%s"'%date)
                            if date[:11] == 'Aujourd\'hui' :
                                hourMin = datetime.strptime(date[13:18],"%Hh%M")
                                pubDate.replace(hour=hourMin.hour,minute=hourMin.minute,second=0,microsecond=0)
                                break
                            elif date[:4] == 'Hier' :
                                pubDate = datetime.today()-timedelta(days=1)
                                hourMin = datetime.strptime(date[6:11],"%Hh%M")
                                pubDate.replace(hour=hourMin.hour,minute=hourMin.minute,second=0,microsecond=0)
                                break
                            else :
                                locale.setlocale(locale.LC_ALL, ('fr_FR', 'UTF8'))
                                pubDate = datetime.strptime(date,"%a, %d %b %Y, %Hh%M")
                                break
                        except :
                            pass
                    locale.setlocale(locale.LC_ALL, ('en_US', 'UTF8'))
                    itemOutputNode.appendChild(domOutput.createNode('pubDate',pubDate.strftime('%a, %d %b %Y %H:%M:%S CEST')))

                    try:
                        description = divVideoNode.find('p',attrs={'class':'teaserText'}).string
                    except :
                        description = ''
                    itemOutputNode.appendChild(domOutput.createNode('description',description))

                    mediaOutputNode = itemOutputNode.appendChild(domOutput.createNode('media:group'))
                    thumbnailNode = divVideoNode.find('img',attrs={'class':'thumbnail'})
                    if thumbnailNode :
                        thumbnail = urlSite+thumbnailNode.get('src')
                    else :
                        thumbnail = 'http://www.arte.tv/header-include/assets/img/arte-tv.png' #parfois le code html est invalide et beautifusoup ne trouve pas l'image
                    mediaOutputNode.appendChild(domOutput.createNode(
                            tag='media:thumbnail',
                            attributes={
                                    'url':thumbnail
                        }))
                    durationNode = divVideoNode.find('div',attrs={'class':'duration_thumbnail'})
                    if durationNode :
                        'Durée connue'
                        (durationMin,durationSec) = divVideoNode.find('div',attrs={'class':'duration_thumbnail'}).string.split(':')
                        mediaOutputNode.appendChild(domOutput.createNode(
                                tag='media:content',
                                attributes={
                                        'url':link,
                                        'duration': str(int(durationMin)*60 + int(durationSec))
                            }))
                    else :
                        'Durée non connue'
                        mediaOutputNode.appendChild(domOutput.createNode(
                                tag='media:content',
                                attributes={
                                        'url':link,
                            }))

                    divRating = divVideoNode.find('div',attrs={'class':'rateContainer'}).findAll('div',attrs={'class':re.compile('star-rating-on')})
                    itemOutputNode.appendChild(domOutput.createNode('ratting',str(len(divRating))))
                except Exception, e :
                    logger.warning("%s in div\n%s\n(url: %s)"%(e,divVideoNode,urlPage))
                    exit

    return domOutput


def domSearch (text,page) :
    '''
        Retourne un domDocument pour la recherche de MythNetVision
    '''
    domOutput = minidom.Document()
    nodeOutputRss = domOutput.appendChild(domOutput.createNode(
            tag='rss',
            attributes={
                    'version':'2.0',
                    'xmlns:itunes':'http://www.itunes.com/dtds/podcast-1.0.dtd',
                    'xmlns:content':'http://purl.org/rss/1.0/modules/content/',
                    'xmlns:cnettv':'http://cnettv.com/mrss/',
                    'xmlns:creativeCommons':'http://cnettv.com/mrss/',
                    'xmlns:media':'http://search.yahoo.com/mrss/',
                    'xmlns:atom':'http://www.w3.org/2005/Atom',
                    'xmlns:amp':'http://www.adobe.com/amp/1.0',
                    'xmlns:dc':'http://purl.org/dc/elements/1.1/',
                    'xmlns:mythtv':'http://www.mythtv.org/wiki/MythNetvision_Grabber_Script_Format'
                }))

    channelOutputNode = nodeOutputRss.appendChild(domOutput.createNode('channel'))
    channelOutputNode.appendChild(domOutput.createNode('title',__title__))
    channelOutputNode.appendChild(domOutput.createNode('link',channel['link']))
    channelOutputNode.appendChild(domOutput.createNode('description',channel['description']))

    url = urlSearch%(urlencode({'q':search.encode('iso-8859-1'),'pageNr':page}))
    logger.info('Recherche sur "%s"'%url)
    soup = BeautifulSoup(urlopen(url).read().replace('&#39;','\''))

    divContentVideoNode = soup.find('div',attrs={'class':'newVideos'})
    lstDivVideosNodes = divContentVideoNode.findAll('div',attrs={'class':'video'})
    #Page
    numresults = soup.find(id='channelMusique').find('h2',attrs={'class':'search'}).find('span').string
    channelOutputNode.appendChild(domOutput.createNode('numresults',numresults))
    nbParPage = len(lstDivVideosNodes)
    channelOutputNode.appendChild(domOutput.createNode('returned',str(nbParPage)))
    startIndex = (page-1)*nbParPage+1
    channelOutputNode.appendChild(domOutput.createNode('startindex',str(startIndex)))
    logger.info('Affichage de la page %i de recherche (1er: %i; nb:%i; tot:%s)'%(page,startIndex,nbParPage,numresults))

    for divVideoNode in lstDivVideosNodes :
        itemOutputNode = channelOutputNode.appendChild(domOutput.createNode('item'))

        titleInputNode = divVideoNode.find('h2').find('a')
        itemOutputNode.appendChild(domOutput.createNode('title',titleInputNode.string))

        link = urlSite+titleInputNode.get('href')
        itemOutputNode.appendChild(domOutput.createNode('link',link))

        pubDate = datetime.today().replace(hour=0,minute=0,second=0,microsecond=0)
        for dateNode in divVideoNode.findAll('p') :
            date = dateNode.string
            try :
                logger.debug('    Parse date "%s"'%date)
                if date[:11] == 'Aujourd\'hui' :
                    hourMin = datetime.strptime(date[13:18],"%Hh%M")
                    pubDate.replace(hour=hourMin.hour,minute=hourMin.minute,second=0,microsecond=0)
                    break
                elif date[:4] == 'Hier' :
                    pubDate = datetime.today()-timedelta(days=1)
                    hourMin = datetime.strptime(date[6:11],"%Hh%M")
                    pubDate.replace(hour=hourMin.hour,minute=hourMin.minute,second=0,microsecond=0)
                    break
                else :
                    locale.setlocale(locale.LC_ALL, ('fr_FR', 'UTF8'))
                    pubDate = datetime.strptime(date,"%a, %d %b %Y, %Hh%M")
                    break
            except :
                pass
        locale.setlocale(locale.LC_ALL, ('en_US', 'UTF8'))
        itemOutputNode.appendChild(domOutput.createNode('pubDate',pubDate.strftime('%a, %d %b %Y %H:%M:%S CEST')))

        try:
            description = divVideoNode.find('p',attrs={'class':'teaserText'}).string
        except :
            description = ''
        itemOutputNode.appendChild(domOutput.createNode('description',description))

        mediaOutputNode = itemOutputNode.appendChild(domOutput.createNode('media:group'))
        mediaOutputNode.appendChild(domOutput.createNode(
                tag='media:thumbnail',
                attributes={
                        'url':urlSite+divVideoNode.find('img',attrs={'class':'thumbnail'}).get('src')
            }))
        durationNode = divVideoNode.find('div',attrs={'class':'duration_thumbnail'})
        if durationNode :
            'Durée connue'
            (durationMin,durationSec) = divVideoNode.find('div',attrs={'class':'duration_thumbnail'}).string.split(':')
            mediaOutputNode.appendChild(domOutput.createNode(
                    tag='media:content',
                    attributes={
                            'url':link,
                            'duration': str(int(durationMin)*60 + int(durationSec))
                }))
        else :
            'Durée non connue'
            mediaOutputNode.appendChild(domOutput.createNode(
                    tag='media:content',
                    attributes={
                            'url':link,
                }))

        divRating = divVideoNode.find('div',attrs={'class':'rateContainer'}).findAll('div',attrs={'class':re.compile('star-rating-on')})
        itemOutputNode.appendChild(domOutput.createNode('ratting',str(len(divRating))))

    return domOutput


if __name__ == "__main__" :

    #Charge le config utilisateur si elle existe
    config = RawConfigParser()
    if os.path.exists(userConfigFile) :
        config.read(userConfigFile)
    elif os.path.exists(configFile) :
        config.read(configFile)
    if config.has_section('external_commands') :
        if config.has_option('external_commands','player') :
            player = config.get('external_commands','player')
        if config.has_option('external_commands','playerargs') :
            playerargs = config.get('external_commands','playerargs')
        if config.has_option('external_commands','download') :
            player = config.get('external_commands','download')
        if config.has_option('external_commands','downloadargs') :
            playerargs = config.get('external_commands','downloadargs')


    opts,args = getopt(sys.argv[1:], "hvS:Tp:",["help","version","search=","treeview","page="])
    if len(opts) == 0 :
        sys.stdout.write(__usage__)
        sys.exit(0)

    search = False
    page = 1
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            sys.stdout.write(__usage__)
            sys.exit(0)
        elif opt in ("-v", "--version"):
            sys.stdout.write(domVersion().toxml())
            sys.exit(0)
        elif opt in ("-S", "--search"):
            search = arg
        elif opt in ("-p", "--page"):
            page = int(arg)
        elif opt in ("-T", "--treeview"):
            sys.stdout.write(domTreeview().toxml())
            sys.exit(0)

    if search :
        sys.stdout.write(domSearch(search,page).toxml())
        sys.exit(0)
